package com.b2camp.b2campparameterservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class B2campparameterserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(B2campparameterserviceApplication.class, args);
	}

}
